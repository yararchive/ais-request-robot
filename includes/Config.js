var Config = {
  db: {
    AIS: {
      server: "SQLSERVER",
      name: "SQLDBNAME",
      user: "SQLUSERNAME",
      pass: "SQLUSERPASS"
    },
    robot_id: "ASPNETUSERID", // ID ������������ -- ������-����������� � ��
    max_reconnects: 2, // ������� ��� ���������� ������� ����������� � ��. 0 - ����� ������� �� ���������.
    reconnect_interval: 1 // � ��������,
  },
  log: {
    level: ["info", "error", "critical"],
    file: ".\\log\\request-robot.log",
    unique_name: true
  },
  mail: {
    smtpserver: "SMTPSERVER",
    smtpserverport: 25,
    smtpconnectiontimeout: 30,
    smtpusessl: true,
    sendusername: "SMTPSERVERLOGIN",
    sendpassword: "SMTPSERVERPWD",
    fromfield: "SENDERNAME"
  }
};