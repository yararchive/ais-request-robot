var Util = {};

// ��� ID ������, ������ � ���.
Util.ID_cache = {};

// ��� (int(4)) ����������� � ���� (datetime)
// period: 0 - 1 ������, 1 - 31 �������
Util.yearToDate = function(year, period) {
  return year < 1000 || year > 9999 ? "NULL" : "CONVERT(datetime2, '" + year + "-" + (period == 0 ? "01-01" : "12-31") + " 00:00:00', 20)";
};

// delay � �������������
Util.sleep = function(delay) {
  var start_time = +new Date();
  var end_time = delay + +start_time;
  var current_time = null
  do {
    current_time = +new Date();
  } while (current_time <= end_time)
};

// ����������� ������ � ������(�)
Util.wrap = function(src_str, left, right) {
  left = !left && left !== "" ? "'" : left;
  right = right || null;

  return left + src_str + (right || left);
};

// ����������� ��������� ������� � �������
Util.safe = function(src_str) {
  return src_str.replace(/\'/gi, "\"");
};

// ����������� ������ �������� � ���������
Util.not_null = function(src_str, options) {
  options = options || {};
  src_str = String(src_str);
  var empty_src_str = src_str == "" || src_str == "null" || src_str == null || src_str == "undefined" ? true : false;
  var replace_str = options.replace_str || "";
  if (options.wrap) {
    src_str = this.wrap(src_str);
    replace_str = this.wrap(replace_str);
  }
  if (options.safe) {
    src_str = this.safe(src_str);
    replace_str = this.safe(replace_str); 
  }
  return empty_src_str ? replace_str : src_str;
};

Util.str_replace = function(search, replace, subject) {
  // +   Replace all occurrences of the search string with the replacement string
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Gabriel Paderni

  if(!(replace instanceof Array)){
    replace=new Array(replace);
    if(search instanceof Array){//If search is an array and replace is a string, then this replacement string is used for every value of search
      while(search.length>replace.length){
        replace[replace.length]=replace[0];
      }
    }
  }

  if(!(search instanceof Array))search = new Array(search);
  while(search.length>replace.length){//If replace  has fewer values than search , then an empty string is used for the rest of replacement values
    replace[replace.length]='';
  }

  if(subject instanceof Array){//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
    for(k in subject){
      subject[k]=str_replace(search,replace,subject[k]);
    }
    return subject;
  }

  for(var k=0; k<search.length; k++){
    var i = subject.indexOf(search[k]);
    while(i > -1){
      subject = subject.replace(search[k], replace[k]);
      i = subject.indexOf(search[k],i);
    }
  }

  return subject;
};

Util.fund_category_id = function(src_cat) {
  if (src_cat == "" || src_cat == null || typeof src_cat == "undefined") src_cat = "d";
  src_cat = String(src_cat);
  return this.str_replace(
    ["a", "b", "c", "d"],
    ["1", "2", "3", "NULL"],
    src_cat
  );
};

Util.documentation_type_id = function(src_type) {
  src_type = String(src_type);
  if (src_type == "" || src_type == null || src_type == "null" || typeof src_type == "undefined") src_type = "1";
  return this.str_replace(
    ["one", "four", "two", "three", "six", "seven", "five", "eight", "twelve", "nine"],
    [ "1",  "4",    "2",   "3",     "6",   "7",     "5",    "8",     "12",     "9"],
    this.str_replace(
      ["1",   "2",    "3",   "4",     "5",   "6",     "7",    "8",     "9",      "one0"], // one0 ������, ��� 1 ��� ��������� �� one
      ["one", "four", "two", "three", "six", "seven", "five", "eight", "twelve", "nine"],
      src_type
    )
  );
};

Util.value_id = function(src_type) {
  src_type = String(src_type);
  if (src_type == "" || src_type == null || src_type == "null" || typeof src_type == "undefined") src_type = "1";
  return this.str_replace(
    ["a", "b", "c"],
    ["4", "2", "3"],
    src_type
  );
};

Util.media_type_id = function(src_type) {
  src_type = String(src_type);
  if (src_type == "" || src_type == null || src_type == "null" || typeof src_type == "undefined") src_type = "1";
  return this.str_replace(
    ["T", "E"],
    ["1", "2"],
    src_type
  );
};

Util.isDbNull = function(obj) {
  obj = String(obj);
  return obj == "" || obj == null || obj == "null" || typeof obj == "undefined";
};

Util.set_fund_cache = function(options) {
  options = options || {};
  if (options.Number && options.id) {
    this.ID_cache.f[options.id]
  } else {
    return false;
  }
};

Util.cutToLength = function(src_str, dst_length) {
  dst_length = parseInt(dst_length);
  var dst_str = src_str;
  var terminator = " ...";
  if (src_str.length > dst_length) {
    dst_str = src_str.substr(0, dst_length - terminator.length) + terminator;
  }
  return dst_str;
};